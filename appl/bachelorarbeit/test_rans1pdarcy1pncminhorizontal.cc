// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled RANS/Darcy problem (1p2c/1pncmin)
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/methods.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/stokesdarcy/couplingmanager.hh>

#include "darcyproblem.hh"
#include "ransproblem.hh"

namespace Dumux {
namespace Properties {

SET_PROP(RANSTypeTag, CouplingManager)
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, TTAG(DarcyOnePNCminTypeTag)>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

SET_PROP(DarcyOnePNCminTypeTag, CouplingManager)
{
    using Traits = StaggeredMultiDomainTraits<TTAG(RANSTypeTag), TTAG(RANSTypeTag), TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using RANSTypeTag = TTAG(RANSTypeTag);
    using DarcyTypeTag = TTAG(DarcyOnePNCminTypeTag);

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using DarcyGridManager = Dumux::GridManager<typename GET_PROP_TYPE(DarcyTypeTag, Grid)>;
    DarcyGridManager darcyGridManager;
    darcyGridManager.init("Darcy"); // pass parameter group

    using RANSGridManager = Dumux::GridManager<typename GET_PROP_TYPE(RANSTypeTag, Grid)>;
    RANSGridManager ransGridManager;
    ransGridManager.init("RANS"); // pass parameter group

    // we compute on the leaf grid view
    const auto& darcyGridView = darcyGridManager.grid().leafGridView();
    const auto& ransGridView = ransGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using RANSFVGridGeometry = typename GET_PROP_TYPE(RANSTypeTag, FVGridGeometry);
    auto ransFvGridGeometry = std::make_shared<RANSFVGridGeometry>(ransGridView);
    ransFvGridGeometry->update();
    using DarcyFVGridGeometry = typename GET_PROP_TYPE(DarcyTypeTag, FVGridGeometry);
    auto darcyFvGridGeometry = std::make_shared<DarcyFVGridGeometry>(darcyGridView);
    darcyFvGridGeometry->update();

    using Traits = StaggeredMultiDomainTraits<RANSTypeTag, RANSTypeTag, DarcyTypeTag>;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(ransFvGridGeometry, darcyFvGridGeometry);

    // the indices
    constexpr auto ransCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto ransFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto darcyIdx = CouplingManager::darcyIdx;

    // initialize the fluidsystem (tabulation)
    GET_PROP_TYPE(RANSTypeTag, FluidSystem)::init();

    // the problem (initial and boundary conditions)
    using RANSProblem = typename GET_PROP_TYPE(RANSTypeTag, Problem);
    auto ransProblem = std::make_shared<RANSProblem>(ransFvGridGeometry, couplingManager);
    using DarcyProblem = typename GET_PROP_TYPE(DarcyTypeTag, Problem);
    auto darcyProblem = std::make_shared<DarcyProblem>(darcyFvGridGeometry, couplingManager);

    // get some time loop parameters
    using Scalar = typename GET_PROP_TYPE(RANSTypeTag, Scalar);
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
//    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
  //      restartTime = getParam<Scalar>("TimeLoop.Restart");

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    timeLoop->setPeriodicCheckPoint(getParam<Scalar>("TimeLoop.EpisodeLength", std::numeric_limits<Scalar>::max()));

    // set timeloop for the subproblems, needed for boundary value variations
    ransProblem->setTimeLoop(timeLoop);
    darcyProblem->setTimeLoop(timeLoop);

    // the solution vector
    Traits::SolutionVector sol;
    sol[ransCellCenterIdx].resize(ransFvGridGeometry->numCellCenterDofs());
    sol[ransFaceIdx].resize(ransFvGridGeometry->numFaceDofs());
    sol[darcyIdx].resize(darcyFvGridGeometry->numDofs());

//    const auto& cellCenterSol = sol[ransCellCenterIdx];
//    const auto& faceSol = sol[ransFaceIdx];

    couplingManager->init(ransProblem, darcyProblem, sol);

    // apply initial solution for instationary problems
    // auxiliary free flow solution vector
    typename GET_PROP_TYPE(RANSTypeTag, SolutionVector) ransSol;
    ransSol[ransCellCenterIdx].resize(sol[ransCellCenterIdx].size());
    ransSol[ransFaceIdx].resize(sol[ransFaceIdx].size());
    ransProblem->applyInitialSolution(ransSol);
    ransProblem->updateStaticWallProperties();
    ransProblem->updateDynamicWallProperties(ransSol);

    auto solRANSOld = ransSol;
    sol[ransCellCenterIdx] = ransSol[ransCellCenterIdx];
    sol[ransFaceIdx] = ransSol[ransFaceIdx];

    darcyProblem->applyInitialSolution(sol[darcyIdx]);
    auto solDarcyOld = sol[darcyIdx];

    auto solOld = sol;

    // couplingManager->init(ransProblem, darcyProblem, sol);

    // the grid variables
    using RANSGridVariables = typename GET_PROP_TYPE(RANSTypeTag, GridVariables);
    auto ransGridVariables = std::make_shared<RANSGridVariables>(ransProblem, ransFvGridGeometry);
    ransGridVariables->init(ransSol, solRANSOld);
    using DarcyGridVariables = typename GET_PROP_TYPE(DarcyTypeTag, GridVariables);
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, darcyFvGridGeometry);
    darcyGridVariables->init(sol[darcyIdx]);
    darcyGridVariables->init(sol[darcyIdx], solDarcyOld);

    // intialize the vtk output module
    const auto ransName = getParam<std::string>("Problem.Name") + "_" + ransProblem->name();
    const auto darcyName = getParam<std::string>("Problem.Name") + "_" + darcyProblem->name();

    StaggeredVtkOutputModule<RANSGridVariables, typename GET_PROP_TYPE(RANSTypeTag, SolutionVector)> ransVtkWriter(*ransGridVariables, ransSol, ransName);
    GET_PROP_TYPE(RANSTypeTag, VtkOutputFields)::init(ransVtkWriter);
    ransVtkWriter.write(0.0);

    VtkOutputModule<DarcyGridVariables, typename GET_PROP_TYPE(DarcyTypeTag, SolutionVector)> darcyVtkWriter(*darcyGridVariables, sol[darcyIdx], darcyName);
    using DarcyVelocityOutput = typename GET_PROP_TYPE(DarcyTypeTag, VelocityOutput);
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GET_PROP_TYPE(DarcyTypeTag, VtkOutputFields)::init(darcyVtkWriter);

    darcyVtkWriter.addField(darcyProblem->getEqTemperature(), "eqTemperature");
    darcyVtkWriter.addField(darcyProblem->getRRate(), "reactionRate");
    darcyVtkWriter.addField(darcyProblem->getWaterVapor(), "waterVapor");
    darcyProblem-> updateVtkOutput(sol[darcyIdx]);

    darcyVtkWriter.write(0.0);


    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(ransProblem, ransProblem, darcyProblem),
                                                 std::make_tuple(ransFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 ransFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 darcyFvGridGeometry),
                                                 std::make_tuple(ransGridVariables->cellCenterGridVariablesPtr(),
                                                                 ransGridVariables->faceGridVariablesPtr(),
                                                                 darcyGridVariables),
                                                 couplingManager,
                                                 timeLoop);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the primary variable switches used by the sub models
//    using PriVarSwitchTuple = std::tuple<NoPrimaryVariableSwitch, NoPrimaryVariableSwitch, typename GET_PROP_TYPE(DarcyTypeTag, PrimaryVariableSwitch)>;

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(solOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;

        // update the auxiliary free flow solution vector
        ransSol[ransCellCenterIdx] = sol[ransCellCenterIdx];
        ransSol[ransFaceIdx] = sol[ransFaceIdx];
        ransProblem->updateDynamicWallProperties(ransSol);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // advance grid variables to the next time step
        ransGridVariables->advanceTimeStep();
        darcyGridVariables->advanceTimeStep();

        //update the output fields before write
        darcyProblem->updateVtkOutput(sol[darcyIdx]);

        // write vtk output
        if (!hasParam("TimeLoop.EpisodeLength") || timeLoop->isCheckPoint() || timeLoop->finished() || timeLoop->timeStepIndex() == 1){
          ransVtkWriter.write(timeLoop->time());
          darcyVtkWriter.write(timeLoop->time());
        }

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(ransGridView.comm());
    timeLoop->finalize(darcyGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
