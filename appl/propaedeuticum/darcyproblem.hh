// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_DARCY_SUBPROBLEM_HH
#define DUMUX_DARCY_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/box/properties.hh>
#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/discretization/cellcentered/mpfa/properties.hh>

#include <dumux/porousmediumflow/1pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "1pncminspatialparams.hh"
#include "thermochemreaction.hh"
#include <test/porousmediumflow/1pncmin/implicit/modifiedcao.hh>

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2on2.hh>
#include <dumux/material/fluidmatrixinteractions/1p/thermalconductivityaverage.hh>
#include <dumux/material/components/cao2h2.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

namespace Dumux
{
template <class TypeTag>
class DarcySubProblem;

namespace Properties
{
NEW_TYPE_TAG(DarcyOnePNCminTypeTag, INHERITS_FROM(CCTpfaModel, OnePNCMinNI));

// Set the grid type
SET_TYPE_PROP(DarcyOnePNCminTypeTag, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, 2> >);

// Set the problem property
SET_TYPE_PROP(DarcyOnePNCminTypeTag, Problem, Dumux::DarcySubProblem<TypeTag>);

// the fluid system
SET_PROP(DarcyOnePNCminTypeTag, FluidSystem)
{
  using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
  using H2ON2 = FluidSystems::H2ON2<Scalar>;
  static constexpr auto phaseIdx = H2ON2::gasPhaseIdx; // simulate the air phase
  using type = FluidSystems::OnePAdapter<H2ON2, phaseIdx>;
};
SET_PROP(DarcyOnePNCminTypeTag, SolidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ComponentOne = Components::ModifiedCaO<Scalar>;
    using ComponentTwo = Components::CaO2H2<Scalar>;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo>;
};

SET_TYPE_PROP(DarcyOnePNCminTypeTag, SpatialParams, OnePNCminSpatialParams<TypeTag>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(DarcyOnePNCminTypeTag, UseMoles, true);

SET_INT_PROP(DarcyOnePNCminTypeTag, ReplaceCompEqIdx, 3);
}

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);

    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using ReactionRate = ThermoChemReaction;

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;
    using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);
    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };
    enum
    {
        // Indices of the primary variables
        pressureIdx = Indices::pressureIdx, //gas-phase pressure
        H2OIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx), // mole fraction water

        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,

        // Equation Indices
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase Indices
        cPhaseIdx = SolidSystem::comp0Idx,
        phaseIdx = FluidSystem::phase0Idx,

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx
    };

public:
    DarcySubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
      //name_      = getParam<std::string>("Problem.Name");
      FluidSystem::init(/*tempMin=*/473.15,
                        /*tempMax=*/623.0,
                        /*numTemptempSteps=*/25,
                        /*startPressure=*/0,
                        /*endPressure=*/9e6,
                        /*pressureSteps=*/200);
      diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{},
                                                                     getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));

      unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
      permeability_.resize(fvGridGeometry->gridView().size(codim));
      porosity_.resize(fvGridGeometry->gridView().size(codim));
      reactionRate_.resize(fvGridGeometry->gridView().size(codim));
      Teq_.resize(fvGridGeometry->gridView().size(codim));
      waterVapor_.resize(fvGridGeometry->gridView().size(codim));
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const // define output
    { return true; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 280; } // 280°C
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolumeFace &scvf) const
    {
        BoundaryTypes values;

       values.setAllNeumann();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf)){
            values.setCouplingNeumann(temperatureIdx);
        }

        return values;
    }

        /*!
     * \brief Evaluate the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary subcontrolvolumeface
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        const auto& ipGlobal = scvf.ipGlobal();


        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf)){
            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);
        }


        if(onLowerBoundary_(ipGlobal)){

            // set a fixed pressure on the bottom side of the domain
            const Scalar dirichletPressure = 8.7e3;
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];

            FluidState fluidStateBorder;
            fluidStateBorder.setTemperature(318.15);
            fluidStateBorder.setPressure(phaseIdx,  8.7e3);
            fluidStateBorder.setMassFraction(phaseIdx, H2OIdx, 1.0);
            fluidStateBorder.setMassFraction(phaseIdx, H2OIdx-1, 0.0);

            Scalar enthalpyBorder = FluidSystem::enthalpy(fluidStateBorder, phaseIdx);

            // construct the element solution
            const auto elemSol = [&]()
            {
                auto sol = elementSolution(element, elemVolVars, fvGeometry);
                return sol;
            }();

            // evaluate the gradient
            const auto gradient = [&]()->GlobalPosition
            {
                const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();
                const Scalar scvCenterPresureSol = elemSol[0][pressureIdx];
                auto grad = ipGlobal - scvCenter;
                grad /= grad.two_norm2();
                grad *= (dirichletPressure - scvCenterPresureSol);
                return grad;
            }();

            const Scalar K = volVars.permeability();
            const Scalar density = volVars.molarDensity();

            // calculate the flux
            Scalar tpfaFlux = gradient * scvf.unitOuterNormal();
            tpfaFlux *= -1.0  * K;
            tpfaFlux *=  density * volVars.mobility();

            Scalar tpfaFluxMass = gradient * scvf.unitOuterNormal();
            tpfaFluxMass *= -1.0  * K;
            tpfaFluxMass *=  volVars.density() * volVars.mobility();

            values[Indices::energyEqIdx] = tpfaFluxMass * enthalpyBorder;
            values[Indices::conti0EqIdx + 1] = tpfaFlux;
        }

        return values;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The subcontrolvolume
     */
    template<class ElementVolumeVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        const auto& volVars = elemVolVars[scv];

        Scalar qMass = rrate_.thermoChemReactionSimple(volVars);

        Scalar qMole = qMass/FluidSystem::molarMass(H2OIdx)*(1-volVars.porosity());

        source[conti0EqIdx+CaO2H2Idx] = qMole;
        source[conti0EqIdx+CaOIdx] = - qMole;
        source[conti0EqIdx+H2OIdx] = - qMole;

        Scalar deltaH = 112e3; // J/mol
        source[energyEqIdx] = qMole * (deltaH - 4*(volVars.pressure(phaseIdx)/volVars.molarDensity(phaseIdx)));

        return source;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Element &element) const
    {
        PrimaryVariables values(0.0);

        Scalar pInit;
        Scalar tInit;
        Scalar h2oInit;
        Scalar CaOInit;
        Scalar CaO2H2Init;

        pInit = getParam<Scalar>("Problem.PressureInitial");
        tInit = getParam<Scalar>("Problem.TemperatureInitial");
        h2oInit = getParam<Scalar>("Problem.VaporInitial");
        CaOInit = getParam<Scalar>("Problem.CaOInitial");
        CaO2H2Init = getParam<Scalar>("Problem.CaO2H2Initial");

        values[pressureIdx] = pInit;
        values[conti0EqIdx+1]   = h2oInit;
        values[temperatureIdx] = tInit;
        values[CaOIdx] = CaOInit;
        values[CaO2H2Idx]   = CaO2H2Init;

        return values;
    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    /*!
      * \brief Return the permeability
      */
     const std::vector<Scalar>& getPerm()
     {
         return permeability_;
     }

    /*!
      * \brief Return the porosity
      */
     const std::vector<Scalar>& getPoro()
     {
         return porosity_;
     }

      /*!
      * \brief Return the reaction rate
      */
     const std::vector<Scalar>& getRRate()
     {
         return reactionRate_;
     }

     /*!
     * \brief Return the equilibrium temperature
     */
    const std::vector<Scalar>& getEqTemperature()
    {
        return Teq_;
    }

    /*!
    * \brief Return the water vapor
    */
   const std::vector<Scalar>& getWaterVapor()
   {
       return waterVapor_;
   }


     /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
     void updateVtkOutput(const SolutionVector& curSol)
     {
         for (const auto& element : elements(this->fvGridGeometry().gridView()))
         {
              const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

              auto fvGeometry = localView(this->fvGridGeometry());
              fvGeometry.bindElement(element);

              for (auto&& scv : scvs(fvGeometry))
              {
                  VolumeVariables volVars;
                  volVars.update(elemSol, *this, element, scv);
                  const auto dofIdxGlobal = scv.dofIndex();
                  reactionRate_[dofIdxGlobal] = rrate_.thermoChemReactionSimple(volVars);
                  Teq_[dofIdxGlobal] = rrate_.equilibriumTemperature(volVars); //the equilibrium temperature
                  waterVapor_[dofIdxGlobal] = volVars.moleFraction(phaseIdx, H2OIdx); //mole fraction of the water vapor
              }
         }
    }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }

    Scalar eps_;
    std::shared_ptr<CouplingManager> couplingManager_;


    DiffusionCoefficientAveragingType diffCoeffAvgType_;

    std::vector<double> permeability_;
    std::vector<double> porosity_;
    std::vector<double> reactionRate_;
    std::vector<double> Teq_;
    std::vector<double> waterVapor_;

    ReactionRate rrate_;
};
} //end namespace

#endif //DUMUX_DARCY_SUBPROBLEM_HH
