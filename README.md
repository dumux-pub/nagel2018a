Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

N. Nagel<br>
Investigating turbulence in an indirect thermo-chemical heat storage reactor<br>
Bachelor's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 11/2018.


Installation
============

You can build the module just like any other DUNE module. For building and running the executables, please go to the folders
containing the sources listed below. For the basic dependencies see dune-project.org.

The easiest way to install this module is to create a new folder and to execute the file
[installNagel2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/nagel2018a/blob/master/installNagel2018a.sh)
in this folder.

```bash
mkdir -p Nagel2018a && cd Nagel2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/nagel2018a/blob/master/installNagel2018a.sh
chmod +x installNagel2018a.sh
./installNagel2018a.sh
```

For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 3.


Applications
============
The results in the bachelor's thesis are obtained by compiling and running the programs from the sources listed below.

* __propaedeuticum__:
coupling laminar free flow to porous medium flow through the heat flux only, see <br>
appl/propaedeuticum


* __bachelorarbeit__:
coupling turbulent free flow to porous medium flow through the heat flux only, see <br>
appl/bachelorarbeit
